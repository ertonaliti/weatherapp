from main import app
import unittest


class FlaskTestCase(unittest.TestCase):
    #Ensure that flask was set up correctly
    def test_index(self):
        tester = app.test_client(self)
        response = tester.get('/login', content_type='html/text')
        self.assertEqual(response.status_code, 200)

    #Ensure that login page loads correctly
    def test_login_page(self):
        tester = app.test_client(self)
        response = tester.get('/login', content_type='html/text')
        self.assertTrue(b'Login' in response.data)

    #Ensure that login page loads correctly, given right credentials
    def test_login_page_loads(self):
        tester = app.test_client(self)
        response = tester.post('/login', data=dict(email="admin@admin", password="adminadmin"), follow_redirects=True)
        self.assertTrue(b'Logged in successfully!' in response.data)

    #Ensure that login page loads correctly, given right credentials
    def test_incorrect_login(self):
        tester = app.test_client(self)
        response = tester.post('/login', data=dict(email="admin@admin", password="fakefake"), follow_redirects=True)
        self.assertTrue(b'Incorrect password, try again' in response.data)

    # Ensure that login page loads correctly, given right credentials
    def test_logout(self):
        tester = app.test_client(self)
        tester.post('/login', data=dict(email="admin@admin", password="adminadmin"), follow_redirects=True)
        response = tester.get('/logout', follow_redirects=True)
        self.assertEqual(response.status_code, 200)



if __name__ == '__main__':
    unittest.main()
