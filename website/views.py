from flask import Blueprint, render_template, request, flash, redirect, url_for
from .models import City
from flask_login import login_required, current_user
from . import db
import requests

views = Blueprint('views', __name__)


def get_weather_data(city):
    api_url = f'https://api.openweathermap.org/data/2.5/weather?q={city}&units=metric&appid=0e6cb33cd24973e9f6a48b2f6ad67a93'
    r = requests.get(api_url).json()
    return r

@views.route('/')
@login_required
def home_get():

    cities = City.query.filter_by(user_id=current_user.id).all()

    weather_data = []

    for city in cities:
        r = get_weather_data(city.name)

        weather = {
            'city': city.name,
            'temperature': r['main']['temp'],
            'description': r['weather'][0]['description'],
            'icon': r['weather'][0]['icon'],
        }
        weather_data.append(weather)

    return render_template('home.html', user=current_user, weather_data=weather_data)


@views.route('/', methods=['POST'])
@login_required
def home_post():
    new_city = request.form.get('city')

    if new_city:
        existing_city = City.query.filter(City.name.like(new_city),  City.user_id.like(current_user.id)).first()
        if not existing_city:
            check_city = get_weather_data(new_city)
            if check_city['cod'] == 200:
                new_city_obj = City(name=new_city, user_id=current_user.id)
                db.session.add(new_city_obj)
                db.session.commit()
            else:
                flash('City does not exists!', category='error')
        else:
            flash('The city already exists!', category='error')
    flash('Successfully added the city!', 'success')
    return redirect(url_for('views.home_get'))


@views.route('/delete/<name>')
@login_required
def delete_city(name):

    city = City.query.filter(City.name.like(name),  City.user_id.like(current_user.id)).first()
    db.session.delete(city)
    db.session.commit()

    flash(f'Successfully deleted {city.name}', 'success')
    return redirect(url_for('views.home_get'))
