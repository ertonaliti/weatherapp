A minimal web app developed with Flask framework.
The main purpose is to introduce how to implement the essential elements in web application with Flask, including
- URL Building
- Authentication with Sessions
- Template & Template Inheritance
- Error Handling
- Integrating with Bootstrap
- Interaction with Database (SQLite)
- Invoking static resources


How to Run
1. Make sure you have Python
2. Install the dependencies and requirements: pip install -r requirements.txt
3. Go to this app's directory and run python main.py

In flask, Default port is 5000
http://127.0.0.1:5000


Requirements.txt includes
- flask==2.0.1
- flask_sqlalchemy
- flask-login
- requests


Endpoints
- http://127.0.0.1:5000/login - if registered you can authenticate and login to the app 
- http://127.0.0.1:5000/logout - after getting the job done you can log out of your account
- http://127.0.0.1:5000/sign-up - if not registered first you need to fill the form and create an account
- http://127.0.0.1:5000/ - after logged in you will be redirected to homepage where you can see the weather of different cities based on what you search
- http://127.0.0.1:5000/delete/<name> - you can delete a city, <name> stands for the city you want to delete


Details about This WeatherApp
- Only logged-in and authenticated user can access the page.
- After logging in the user can add multiple cities to check the weather.
- An account is set for testing, like email: admin@admin and password: adminadmin.

